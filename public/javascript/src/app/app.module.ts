import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { routing } from './app.routes';


import { AppComponent } from './app.component';
import { MenuComponent } from './menu.component';


import { MenuService } from './menu.service';


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    routing,

  ],
  providers: [
    MenuService
  ],
  bootstrap: [
    AppComponent
  ]

})
export class AppModule {}
