const mongoose = require('mongoose');

var MealSchema = new mongoose.Schema({
  name: String,
  description: String,
  cost: Number,
  type: String,
  isSpecial: Boolean
});

var Meal = mongoose.model('Meal', MealSchema);

module.exports = Meal;
