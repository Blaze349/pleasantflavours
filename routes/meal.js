var express = require('express');
var router = express.Router();
const mongoose = require('mongoose');

mongoose.connect('mongodb://admin:food@ds153729.mlab.com:53729/pleasantflavours', error => {
  console.log(error)
});

const Meal = require('../db/Menu');

/* GET users listing. */
router.get('/api/resources', function(req, res, next) {
  Meal.find({}, (err,docs) => {
    res.json(docs);
  });
});

router.get('/api/resources/type/:type', (req, res, next) => {
  if (req.params.type) {
    Meal.find({type: req.params.type }, (err, docs) => {
      res.json(docs);
    });
  }
});

router.get('/api/resources/time/:time', (req, res, next) => {
  if (req.params.time) {
    Meal.find({time: req.params.time }, (err, docs) => {
      res.json(docs);
    });
  }
});

router.get('/api/resources/specials', (req, res, next) => {

    Meal.find({isSpecial: true }, (err, docs) => {
      res.json(docs);
    });

})

module.exports = router;
