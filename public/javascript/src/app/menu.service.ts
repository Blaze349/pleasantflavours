import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Meal } from './meal';

@Injectable()
export class MenuService {

  private headers = new Headers({'Content-type': 'application/json'});
  private menuUrl = '/api/resources';
  private http: Http;

  getMenuList(): Promise<Meal[]> {
    return this.http.get(this.menuUrl)
        .toPromise()
        .then(response => response.json().data as Meal[])
        .catch((err) => console.log(err));
  }
  getMealsOfType(type: "String"): Promise<Meal[]> {
    const url = '${this.menuUrl}/type/${type}';
    return this.http.get(url).toPromise().then(response => response.json().data as Meal[]).catch(err => console.log("Error has occured"));
  }
  getSpecials(): Promise<Meal[]> {
    const url = '${this.menuUrl}/specials';
    return this.http.get(url).toPromise().then(response => response.json().data as Meal[]).catch(err => console.log("Specials error has occured"));
  }
}
