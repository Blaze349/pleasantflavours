const mongoose = require('mongoose');

mongoose.connect('mongodb://admin:food@ds153729.mlab.com:53729/pleasantflavours', error => {
  console.log(error)
});

const Meal = require('../db/Menu');

var Donut = new Meal({
    name: 'Donut',
    description: 'Deliciously sweet',
    type: 'Goodies',
    isSpecial: false
});

var ScotchEgg = new Meal({
    name: 'Scotch Egg',
    description: 'A firm egg within a meaty exterior',
    type: 'Goodies',
    isSpecial: false
});

var SausageRoll = new Meal({
    name: 'Sausage Roll',
    description: 'Meat within a pastry',
    type: 'Goodies',
    isSpecial: false
});

var MoiMoi = new Meal({
    name: 'Moi-Moi',
    description: 'Traditional bean',
    type: 'Goodies',
    isSpecial: false
});

var Jollof = new Meal({
    name: 'Jollof Rice',
    description: 'Special rice',
    type: 'Rice',
    isSpecial: false
});

var FriedRice = new Meal({
    name: 'Fried Rice',
    description: 'Fried Rice',
    type: 'Rice',
    isSpecial: false
});

var FriedChicken = new Meal({
    name: 'Fried Chicken',
    description: 'Fried chicken taste nice',
    type: 'Meat',
    isSpecial: false
});

var Asun = new Meal({
    name: 'Asun',
    description: 'Smoked goat',
    type: 'Meat',
    isSpecial: false
});

var Egusi = new Meal({
    name: 'Egusi Soup',
    description: 'Egusi Soup',
    type: 'Soup',
    isSpecial: false
});

var VegetableSoup = new Meal({
    name: 'Vegetable Soup',
    description: 'Soup of vegetables',
    type: 'Soup',
    isSpecial: false
});

var Gari = new Meal({
    name: 'Eba / Amala / Semorita',
    description: 'Different types of Gari',
    type: 'Side',
    isSpecial: false
});

var Salad = new Meal({
    name: 'Salad',
    description: 'Traditional nigerian salad',
    type: 'Side',
    isSpecial: false
});

var Grasscutter = new Meal({
    name: 'Grasscutter Pepper Soup',
    description: 'Hot soup from nigerian grasscutter',
    type: 'Special',
    isSpecial: true
});

var FreshFish = new Meal({
    name: 'Fresh Fish Pepper Soup',
    description: 'Hot soup from fresh fish',
    type: 'Special',
    isSpecial: true
});

Donut.save(err => {
  if (err) {
    console.log(err);
  }
});

ScotchEgg.save(err => {
  if (err) {
    console.log(err);
  }
});

MoiMoi.save(err => {
  if (err) {
    console.log(err);
  }
});

Jollof.save(err => {
  if (err) {
    console.log(err);
  }
});
FriedRice.save(err => {
  if (err) {
    console.log(err);
  }
});
FriedChicken.save(err => {
  if (err) {
    console.log(err);
  }
});
Asun.save(err => {
  if (err) {
    console.log(err);
  }
});
Egusi.save(err => {
  if (err) {
    console.log(err);
  }
});
VegetableSoup.save(err => {
  if (err) {
    console.log(err);
  }
});
Gari.save(err => {
  if (err) {
    console.log(err);
  }
});
Salad.save(err => {
  if (err) {
    console.log(err);
  }
});
Grasscutter.save(err => {
  if (err) {
    console.log(err);
  }
});
FreshFish.save(err => {
  if (err) {
    console.log(err);
  }
});

console.log('done')
