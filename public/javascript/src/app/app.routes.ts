
import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuComponent } from './menu.component'

export const AppRoutes: Routes = [
  { path: '/', component: MenuComponent },
  { path: '/menu', component: MenuComponent },

];

export const routing: ModuleWithProviders = RouterModule.forRoot(AppRoutes);
