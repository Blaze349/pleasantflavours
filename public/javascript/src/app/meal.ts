export class Meal {
  _id: String;
  name: String;
  description: String;
  type: String;
  isSpecial: Boolean;
}
